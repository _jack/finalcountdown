//------ Component DinoCount----------------------------------------------------------

var DinoCount = Vue.component("dinoCount", {
  template: `
    <div class="count">
    <div class="count_animBg" v-bind:class="{ 'count_animBg_animBack': animBack }"></div>
       <div class="count_dino" v-bind:class="{ 'timeToDie': timeToDie }">
        <svg width="50px" height="50px" viewBox="13 0 285 306" version="1.1"      	xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <g id="Group-3" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(13.000000, 0.000000)">
        <polygon id="Path" fill="#6c757d" points="256.351585 91.6642857 213.916427 91.6642857 213.916427 80.0385714 284.492795 80.0385714 284.492795 14.3085714 270.198847 14.3085714 270.198847 0 155.400576 0 155.400576 14.3085714 141.553314 14.3085714 141.553314 105.972857 127.259366 105.972857 127.259366 119.834286 106.26513 119.834286 106.26513 134.142857 85.2708934 134.142857 85.2708934 148.451429 70.9769452 148.451429 70.9769452 162.312857 45.0691643 162.312857 45.0691643 148.004286 31.221902 148.004286 31.221902 134.142857 16.9279539 134.142857 16.9279539 105.972857 0.847262248 105.972857 0.847262248 192.718571 14.6945245 192.718571 14.6945245 207.027143 28.9884726 207.027143 28.9884726 220.888571 42.8357349 220.888571 42.8357349 235.197143 57.129683 235.197143 57.129683 249.058571 70.9769452 249.058571 70.9769452 305.398571 101.351585 305.398571 101.351585 289.301429 87.5043228 289.301429 87.5043228 277.228571 101.351585 277.228571 101.351585 263.367143 115.645533 263.367143 115.645533 249.058571 127.259366 249.058571 127.259366 263.367143 141.553314 263.367143 141.553314 305.398571 171.927954 305.398571 171.927954 289.301429 157.634006 289.301429 157.634006 235.197143 171.927954 235.197143 171.927954 220.888571 185.775216 220.888571 185.775216 199.872857 200.069164 199.872857 200.069164 150.687143 211.682997 150.687143 211.682997 164.548571 228.210375 164.548571 228.210375 134.142857 200.069164 134.142857 200.069164 108.208571 256.351585 108.208571"></polygon>
        <rect id="Rectangle" fill="#FFFFFF" x="169.247839" y="20.5685714" width="16.9740634" 			height="16.9914286"></rect>
        		</g>
        	</svg>
     		</div>
      <div class="count_title">{{ slogan }}</div>
    <div class="count_counter">{{ this.countProgress }}</div>
  </div>
  `,

  props: {
	// no props
  },

  data() {
    return {
      countProgress: 0,
      timeToDie: false,
      animBack: false,
      slogan: 'Время вымирать через:'

    };
  },

  methods: {

    startCount() {

      let max = 7;
      let min = 2;
      let randomAge = Math.floor(Math.random() * (max - min + 1)) + min;
      let responseTime = new Date(Date.now() + 1000 * randomAge); // таймер randomAge секунд
      let countdown = new Date();

      var anim = () => {
        countdown.setTime(responseTime - Date.now());
        this.countProgress = "0" + countdown.getUTCSeconds() + "." + countdown.getUTCMilliseconds();
        if (countdown.getUTCSeconds() > 0) {
          requestAnimationFrame(anim);
        } else {
          this.finishHandler(new Date().toLocaleTimeString())
        }
      }
      requestAnimationFrame(anim);
    },

    finishHandler(time) {
      this.timeToDie = true
      this.animBack = true
      this.slogan = "Этот парень вымер. Время смерти:"
      this.countProgress = time
    }

  },

  mounted: function() {
    this.startCount()
  }

});

//------ VUE ---------------------------------------------------------------

new Vue({
  el: "#app",
  data: {
    counters: [{
      id: 0
    }],
    greeting: "<div class='logo'>Final countdown</div>",
    timeNow: new Date().toLocaleTimeString(),
    currentId: 1
  },
  components: {
    DinoCount
  },

  methods: {

    addCount() {
      this.counters.push({
        id: this.currentId
      })
      this.currentId++
      return this.counters.sort((a, b) => b.id - a.id)
    },

    clearScene() {
      this.counters = []
    },

    plus100() {
      for (let i=0; i<10; i++) {
        setTimeout( () => {
          this.addCount()
        }, i*100 );
      }
    },

  },
  created: function() {
    // время
    setInterval(() => {
      this.timeNow = new Date().toLocaleTimeString();
    }, 1000);
  }
});

//------ просто на JS счетчик --------------------------------------------

let freshBtn = document.querySelector(".fresh");
freshBtn.addEventListener("click", onCleanJS);

function onCleanJS() {

  let max = 5;
  let min = 2;
  let randomAge = Math.floor(Math.random() * (max - min + 1)) + min;

  let timeElem = document.getElementById("time"),
    countdown = new Date(),
    responseTime = new Date(Date.now() + 1000 * randomAge); // таймер на randomAge секунд

  function startTime() {
    countdown.setTime(responseTime - Date.now());
    timeElem.innerHTML =
      countdown.getUTCSeconds() + "." + countdown.getUTCMilliseconds();
    if (
      //   countdown.getUTCHours() > 0 ||
      //   countdown.getUTCMinutes() > 0 ||
      countdown.getUTCSeconds() > 0
    ) {
      requestAnimationFrame(startTime);
    } else {
      timeElem.innerHTML = "Точное время: " + new Date().toLocaleTimeString();
    }
  }
  requestAnimationFrame(startTime);
}